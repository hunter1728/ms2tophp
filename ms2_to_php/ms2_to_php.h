
// ms2_to_php.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CMs2ConverterApp:
// See ms2_to_php.cpp for the implementation of this class
//

class CMs2ConverterApp : public CWinApp
{
public:
	CMs2ConverterApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CMs2ConverterApp theApp;