#include "stdafx.h"
#include "Ms2ToPhpConverter.h"

/***********************************************/
/***********************************************/
CString ConvertMs2ToPhp(const CString & sMs2)
{
	CString sResult = _T("<?php\r\n\r\ninclude('qinit2.php');\r\ninclude('qsqlerr.php');\r\n\r\n");


	int nTokenPos = 0;
	int nLastTokenPos = 0;
	CString sLine = sMs2.Tokenize(_T("\r\n"), nTokenPos);
	while (!sLine.IsEmpty())
	{
		// Check for skipped blank lines
		int nPosDifference = nTokenPos - nLastTokenPos;
		nPosDifference -= 2; // normal crlf
		nPosDifference -= sLine.GetLength();

		if (nPosDifference > 0)
		{
			// add in skipped blank lines
			for (int x = 0; x < nPosDifference / 2; x++)
			{
				sResult += "\r\n";
			}
		}

		// Add the linefeed back in
		sLine.TrimRight();

		sResult += ConvertMs2ToPhpLine(sLine);
		sResult += "\r\n";

		nLastTokenPos = nTokenPos;
		sLine = sMs2.Tokenize(_T("\r\n"), nTokenPos);
	}

	return sResult;
}


/***********************************************/
/***********************************************/
CString ConvertMs2ToPhpLine(const CString & sMs2Line)
{
	CString sPhpLine = sMs2Line;

	FixPrint(sPhpLine);

	FixForm(sPhpLine);
	FixGetCookie(sPhpLine);

	FixComment(sPhpLine);

	FixElse(sPhpLine);
	FixEndIf(sPhpLine);

	FixMySql(sPhpLine);

	FixSemicolons(sPhpLine);

	FixConditions(sPhpLine);

	return sPhpLine;
}

/***********************************************/
/***********************************************/
void FixPrint(CString & sLine)
{
	// If the line has a 'print(' in it, replace it with 'print '
	if (sLine.Replace(_T("print("), _T("print ")))
	{
		// if last character is a ), replace it with a ;
		if (sLine[sLine.GetLength() - 1] == ')')
		{
			sLine.SetAt(sLine.GetLength() - 1, ';');
		}
	}
}

/***********************************************/
/***********************************************/
void FixForm(CString & sLine)
{
	// what if the form line is embedded in another function?

	// While we're polishing, keep the original
	CString sOriginal = _T("// ") + sLine + "\r\n";

	// If the line has a 'print(' in it, replace it with 'print '
	if (sLine.Replace(_T(" = form(\""), _T(" = $_REQUEST['")))
	{
		// Assume first part is the variable name
		sLine.Insert(0, '$');

		// Stick the original in (for now)
		sLine.Insert(0, sOriginal);

		// if last character is a ), replace it with a ;
		if (sLine[sLine.GetLength() - 1] == ')')
		{
			sLine.Replace(_T("\")"), _T("'];"));
		}
	}
}

/***********************************************/
/***********************************************/
void FixGetCookie(CString & sLine)
{
	// what if the form line is embedded in another function?

	// While we're polishing, keep the original
	CString sOriginal = _T("// ") + sLine + "\r\n";

	// If the line has a 'print(' in it, replace it with 'print '
	if (sLine.Replace(_T(" = getcookie(\""), _T(" = $_COOKIE['")))
	{
		// Assume first part is the variable name
		sLine.Insert(0, '$');

		// Stick the original in (for now)
		sLine.Insert(0, sOriginal);

		// if last character is a ), replace it with a ;
		if (sLine[sLine.GetLength() - 1] == ')')
		{
			sLine.Replace(_T("\")"), _T("'];"));
		}
	}
}

/***********************************************/
/***********************************************/
void FixComment(CString & sLine)
{
	CString sTrimmedLine = sLine;
	sTrimmedLine.TrimLeft();
	int nInx = sTrimmedLine.Find(';');
	if (nInx == 0)
	{
		nInx = sLine.Find(';');
		sLine.Delete(nInx, 1);
		sLine.Insert(nInx, _T("//"));
	}
}


/***********************************************/
/***********************************************/
void FixElse(CString & sLine)
{
	sLine.Replace(_T("else"), _T("}\r\nelse\r\n{"));
}

/***********************************************/
/***********************************************/
void FixEndIf(CString & sLine)
{
	sLine.Replace(_T("endif"), _T("}"));
	sLine.Replace(_T("endwhile"), _T("}"));
}


/***********************************************/
/***********************************************/
void FixMySql(CString & sLine)
{
	if (sLine.Replace(_T("mymysqlquery"), _T("mymysql_query")))
	{
		sLine.Replace(_T("handle,"), _T(""));
		sLine.Insert(0, _T("$result = "));
		sLine += ";";
	}
	// cheater string find
	if (sLine.Replace(_T("mysqlstoreresult"), _T("")))
	{
		sLine = "";
	}
}

/***********************************************/
/***********************************************/
void FixSemicolons(CString & sLine)
{
	// only cherry-pick our common ones
	sLine.Replace(_T("exit"), _T("exit;"));
	sLine.Replace(_T("printheader()"), _T("printheader();"));
	sLine.Replace(_T("printfooter()"), _T("printfooter();"));

	CString sTrimmedLine = sLine;
	sTrimmedLine.TrimLeft();
	if (sTrimmedLine.Left(5) == _T("init("))
	{
		sLine += ";";
	}
}


/***********************************************/
/***********************************************/
void FixConditions(CString & sLine)
{
	CString sTrimmedLine = sLine;
	sTrimmedLine.TrimLeft();

	if (sTrimmedLine.Left(3) == _T("if(")
		||
		sTrimmedLine.Left(6) == _T("while("))
	{
		sLine += "\r\n{";
	}
}
