#pragma once
#include "afxwin.h"

CString ConvertMs2ToPhp(const CString & sMs2);
CString ConvertMs2ToPhpLine(const CString & sMs2);

void FixComment(CString & sLine);
void FixPrint(CString & sLine);
void FixForm(CString & sLine);
void FixGetCookie(CString & sLine);
void FixEndIf(CString & sLine);
void FixElse(CString & sLine);
void FixMySql(CString & sLine);
void FixSemicolons(CString & sLine);
void FixConditions(CString & sLine);